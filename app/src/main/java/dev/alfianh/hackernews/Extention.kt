package dev.alfianh.hackernews

import android.view.View
import java.text.SimpleDateFormat
import java.util.*

fun String?.orDefault(): String {
  return if (this.isNullOrBlank()) {
    "-"
  } else {
    this
  }
}

fun View.showView() {
  this.visibility = View.VISIBLE
}

fun View.hideView() {
  this.visibility = View.GONE
}

fun Date.toSimpleFormat(): String {
  return SimpleDateFormat("dd/MM/yyyy").format(this)
}