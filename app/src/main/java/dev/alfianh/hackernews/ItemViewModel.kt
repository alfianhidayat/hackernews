package dev.alfianh.hackernews

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import io.reactivex.Observable
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

class ItemViewModel: ViewModel() {

  private val _loading: MutableLiveData<Boolean> = MutableLiveData()
  private val _stories: MutableLiveData<List<Item>> = MutableLiveData()
  private val _comments: MutableLiveData<List<Item>> = MutableLiveData()

  val isLoading: LiveData<Boolean> = _loading

  val stories: LiveData<List<Item>> = _stories

  val comments: LiveData<List<Item>> = _comments

  fun getStories(page: Int) {
    val take: Long = 10 * page.toLong()
    _loading.postValue(true)
    RetrofitClient.getService().getTopStoryIds()
        .flatMapIterable { it }
        .take(take)
        .takeLast(10)
        .flatMapSingle { id -> getDetailById(id) }
        .toList()
        .subscribeOn(Schedulers.io())
        .observeOn(AndroidSchedulers.mainThread())
        .doOnError { t -> Log.e("Error when getStories", t.message) }
        .subscribe { stories, _ ->
          _stories.postValue(stories.orEmpty())
          _loading.postValue(false)
        }
  }

  fun getComments(story: Item) {
    if (story.kids.isEmpty()) {
      return
    }
    Observable.just(story.kids)
        .flatMapIterable { it }
        .flatMapSingle { id -> getDetailById(id) }
        .toList()
        .subscribeOn(Schedulers.io())
        .observeOn(AndroidSchedulers.mainThread())
        .doOnError { t -> Log.e("Error when getComments", t.message) }
        .subscribe { comments, _ ->
          _comments.postValue(comments.orEmpty())
        }
  }

  private fun getDetailById(id: String): Single<Item> {
    return RetrofitClient.getService().getDetailById(id)
  }

}