package dev.alfianh.hackernews

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView

class CommentAdapter :
    RecyclerView.Adapter<CommentAdapter.MovieViewHolder>() {

  private var mItems = mutableListOf<Item>()

  fun setList(list: List<Item>) {
    mItems.clear()
    mItems.addAll(list)
    notifyDataSetChanged()
  }

  override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MovieViewHolder {
    return MovieViewHolder(
        LayoutInflater.from(parent.context).inflate(R.layout.comment_item, parent, false))
  }

  override fun getItemCount() = mItems.size

  override fun onBindViewHolder(holder: MovieViewHolder, position: Int) {
    val comment = mItems[position]
    holder.tvComment.text = comment.text
  }

  class MovieViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
    val tvComment: TextView = itemView.findViewById(R.id.comment)
  }

}