package dev.alfianh.hackernews

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import dev.alfianh.hackernews.DetailStoryActivity.Companion.FAVORITE_KEY
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

  lateinit var viewModel: ItemViewModel
  lateinit var storyAdapter: StoriesAdapter
  var items: MutableList<Item> = mutableListOf()
  var isLoading = false
  var currentPage = 1

  override fun onCreate(savedInstanceState: Bundle?) {
    super.onCreate(savedInstanceState)
    setContentView(R.layout.activity_main)
    viewModel = ViewModelProviders.of(this).get(ItemViewModel::class.java)
    storyAdapter = StoriesAdapter(items) {
      DetailStoryActivity.start(this, it)
    }
    rvNews.layoutManager = GridLayoutManager(this, 2)
    rvNews.adapter = storyAdapter

    viewModel.stories.observe(this, Observer {
      if (currentPage == 1) {
        items.clear()
      }
      items.addAll(it)
      storyAdapter.notifyDataSetChanged()
      isLoading = false
    })
    viewModel.getStories(currentPage)

    viewModel.isLoading.observe(this, Observer {
      if (it) {
        pbLoading.showView()
      } else {
        pbLoading.hideView()
      }
    })

    initScrollListener()
  }

  private fun initScrollListener() {
    rvNews.addOnScrollListener(object : RecyclerView.OnScrollListener() {
      override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
        val linearLayoutManager = recyclerView.layoutManager as LinearLayoutManager

        if (!isLoading) {
          if (linearLayoutManager != null && linearLayoutManager.findLastCompletelyVisibleItemPosition() == items.size - 1) {
            viewModel.getStories(++currentPage)
            isLoading = true
          }
        }
      }
    })
  }

  override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
    super.onActivityResult(requestCode, resultCode, data)
    if (requestCode == 1) {
      data?.getStringExtra(FAVORITE_KEY)?.let {
        tvTitleLastFav.text = "My Favorite : ${it.orDefault()}"
      }
    }
  }
}
