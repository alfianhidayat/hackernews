package dev.alfianh.hackernews

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Item(
    val by: String? = null,
    val descendants: Int? = null,
    val id: Int? = null,
    val score: Int = 0,
    val text: String? = null,
    val time: Long = 0,
    val title: String? = null,
    val type: String? = null,
    val kids: List<String> = mutableListOf()
): Parcelable