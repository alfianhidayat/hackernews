package dev.alfianh.hackernews

import io.reactivex.Observable
import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Path

interface HackerNewsService {

  @GET("v0/topstories.json?print=pretty")
  fun getTopStoryIds(): Observable<List<String>>

  @GET("v0/item/{id}.json?print=pretty")
  fun getDetailById(@Path("id") id: String): Single<Item>

}