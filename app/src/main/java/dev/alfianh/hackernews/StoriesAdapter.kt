package dev.alfianh.hackernews

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView

class StoriesAdapter(var items: MutableList<Item>, var listener: (story: Item) -> Unit) :
    RecyclerView.Adapter<StoriesAdapter.MovieViewHolder>() {

  override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MovieViewHolder {
    return MovieViewHolder(
        LayoutInflater.from(parent.context).inflate(R.layout.story_item, parent, false))
  }

  override fun getItemCount() = items.size

  override fun onBindViewHolder(holder: MovieViewHolder, position: Int) {
    val story = items[position]
    holder.tvTitle.text = story.title
    holder.tvComment.text = "Total Comment : ${story.kids.size}"
    holder.tvScore.text = "Score : ${story.score}"
    holder.itemView.setOnClickListener {
      listener(story)
    }
  }

  class MovieViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
    val tvTitle: TextView = itemView.findViewById(R.id.title)
    val tvComment: TextView = itemView.findViewById(R.id.comment)
    val tvScore: TextView = itemView.findViewById(R.id.score)
  }

}