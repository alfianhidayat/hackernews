package dev.alfianh.hackernews

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import kotlinx.android.synthetic.main.activity_detail_story.*
import java.util.*

class DetailStoryActivity : AppCompatActivity() {

  lateinit var viewModel: ItemViewModel
  lateinit var commentAdapter: CommentAdapter

  companion object {
    const val STORY_KEY = "STORY_ITEM"
    const val FAVORITE_KEY = "FAVORITE_KEY"
    fun start(context: Context, story: Item) {
      (context as Activity).startActivityForResult(Intent(context, DetailStoryActivity::class.java).apply {
        putExtra(STORY_KEY, story)
      }, 1)
    }
  }

  override fun onCreate(savedInstanceState: Bundle?) {
    super.onCreate(savedInstanceState)
    setContentView(R.layout.activity_detail_story)
    viewModel = ViewModelProviders.of(this).get(ItemViewModel::class.java)
    intent.getParcelableExtra<Item>(STORY_KEY)?.let { item ->
      tvAuthor.text = "By ${item.by.orDefault()}"
      tvTitle.text = item.title.orDefault()
      tvDesc.text = item.text.orDefault()
      tvDate.text = Date(item.time).toSimpleFormat()
      viewModel.getComments(item)
      ivFav.setOnClickListener {
        setResult(1, Intent().apply { putExtra(FAVORITE_KEY, item.title.orDefault()) })
        finish()
      }
    }
    commentAdapter = CommentAdapter()
    rvComment.layoutManager = LinearLayoutManager(this)
    rvComment.adapter = commentAdapter
    viewModel.comments.observe(this, androidx.lifecycle.Observer {
      commentAdapter.setList(it)
    })
  }

}
