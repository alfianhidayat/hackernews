package dev.alfianh.hackernews

import android.util.Log
import io.reactivex.schedulers.Schedulers
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory

object RetrofitClient {

  fun getService(): HackerNewsService {
    val httpInterceptor = HttpLoggingInterceptor(HttpLoggingInterceptor.Logger {
      Log.i("HackerNews", it)
    })
    httpInterceptor.level = HttpLoggingInterceptor.Level.BODY
    val okHttpClient = OkHttpClient.Builder().addInterceptor(httpInterceptor).build()
    val retrofit = Retrofit.Builder().baseUrl("https://hacker-news.firebaseio.com/")
        .addConverterFactory(GsonConverterFactory.create())
        .addCallAdapterFactory(RxJava2CallAdapterFactory.createWithScheduler(Schedulers.io()))
        .client(okHttpClient)
        .build()
    return retrofit.create(HackerNewsService::class.java)
  }

}